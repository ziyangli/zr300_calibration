# README #

A dataset and calibration results (intrinsic and extrinsic) for Intel ZR300 and DJI Matrice 100.
In order to perform the calibration, simply run "all_calibration.sh" in run2 folder.
We assume that you have already installed [Kalibr](https://github.com/ethz-asl/kalibr/wiki) toolbox.